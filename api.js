const list = [
    { name: 'Murugesh Pandian V', age: 24 },
    { name: 'SudharsunKP', age: 24 },
    { name: 'Balaji', age: 24 }
  ]
  
  export default () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        return resolve(list)
      }, 0)
    })
  }